# encoding: utf-8
#--
#   Copyright (C) 2012 Gitorious AS
#   Copyright (C) 2009 Nokia Corporation and/or its subsidiary(-ies)
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU Affero General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU Affero General Public License for more details.
#
#   You should have received a copy of the GNU Affero General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++
require "fast_test_helper"
require "gitorious/ssh/strainer"
require "gitorious/ssh/client"

class RepositoryRoot
  def self.default_base_path; "/tmp"; end
end

class SSHClientTest < MiniTest::Spec
  before do
    @strainer = Gitorious::SSH::Strainer.new("git-upload-pack 'foo/bar.git'").parse!
    @real_path = "abc/123/defg.git"
    @full_real_path = File.join(RepositoryRoot.default_base_path, @real_path)
    @ok_stub = stub("ok response mock",
      :body => "real_path:#{@real_path}\nforce_pushing_denied:false")
    @not_ok_stub = stub("ok response mock", :body => "nil")
  end

  it "parses the project name from the passed in Strainer" do
    client = Gitorious::SSH::Client.new(@strainer, "johan")
    assert_equal "foo", client.project_name
  end

  it "parses the repository name from the passed in Strainer" do
    client = Gitorious::SSH::Client.new(@strainer, "johan")
    assert_equal "bar", client.repository_name
  end

  describe "namespacing" do
    before do
      @team_strainer = Gitorious::SSH::Strainer.new("git-upload-pack '+foo/bar/baz.git'").parse!
      @user_strainer = Gitorious::SSH::Strainer.new("git-upload-pack '~foo/bar/baz.git'").parse!
    end

    it "parses the project name from a team namespaced repo" do
      client = Gitorious::SSH::Client.new(@team_strainer, "johan")
      assert_equal "bar", client.project_name
      assert_equal "baz", client.repository_name
    end

    it "parses the project name from a user namespaced repo" do
      client = Gitorious::SSH::Client.new(@user_strainer, "johan")
      assert_equal "bar", client.project_name
      assert_equal "baz", client.repository_name
    end
  end

  it "sets the username that was passed into it" do
    client = Gitorious::SSH::Client.new(@strainer, "johan")
    assert_equal "johan", client.user_name
  end

  it "returns the correct authentication URL" do
    client = Gitorious::SSH::Client.new(@strainer, "johan")
    assert_equal Gitorious.client.url("/foo/bar/writable_by?username=johan"), client.writable_by_query_url
  end

  describe "configuration parsing" do
    it "parses the basic configuration format" do
      client = Gitorious::SSH::Client.new(@strainer, "johan")
      connection_stub = stub_everything("connection stub")
      connection_stub.expects(:get).with("/foo/bar/config?username=johan").returns(@ok_stub)
      client.expects(:connection).once.returns(connection_stub)
      assert_equal @real_path, client.configuration["real_path"]
      assert_equal "false", client.configuration["force_pushing_denied"]
    end

    it "knows if force_pushing is allowed" do
      client = Gitorious::SSH::Client.new(@strainer, "johan")
      connection_stub = stub_everything("connection stub")
      connection_stub.expects(:get).with("/foo/bar/config?username=johan").returns(@ok_stub)
      client.expects(:connection).once.returns(connection_stub)
      assert !client.force_pushing_denied?
    end

    it "asks gets the real path from the query url" do
      client = Gitorious::SSH::Client.new(@strainer, "johan")
      connection_stub = stub_everything("connection_stub")
      connection_stub.expects(:get) \
        .with("/foo/bar/config?username=johan") \
        .returns(@ok_stub)
      client.expects(:connection).once.returns(connection_stub)
      File.expects(:exist?).with(@full_real_path).returns(true)
      assert_equal @full_real_path, client.real_path
    end

    it "raises if the pre-receive hook is not executable" do
      client = Gitorious::SSH::Client.new(@strainer, "johan")
      client.stubs(:real_path).returns("/tmp/foo.git")
      File.expects(:"executable?").with("/tmp/foo.git/hooks/pre-receive").returns(false)
      assert !client.pre_receive_hook_exists?
    end

    it "raises if the real path does not exist" do
      client = Gitorious::SSH::Client.new(@strainer, "johan")
      connection_stub = stub_everything("connection_stub")
      connection_stub.expects(:get) \
        .with("/foo/bar/config?username=johan") \
        .returns(@ok_stub)
      client.expects(:connection).once.returns(connection_stub)
      File.expects(:exist?).with(@full_real_path).returns(false)
      assert_raises(Gitorious::SSH::AccessDeniedError) do
        client.real_path
      end
    end

    it "raises if the real path is not returned" do
      client = Gitorious::SSH::Client.new(@strainer, "johan")
      connection_stub = stub_everything("connection_stub")
      connection_stub.expects(:get) \
        .with("/foo/bar/config?username=johan") \
        .returns(@not_ok_stub)
      client.expects(:connection).once.returns(connection_stub)
      assert_raises(Gitorious::SSH::AccessDeniedError) do
        client.real_path
      end
    end
  end

  it "returns the command we can safely execute with git-shell" do
    client = Gitorious::SSH::Client.new(@strainer, "johan")
    connection_stub = stub_everything("connection_stub")
    connection_stub.expects(:get) \
      .with("/foo/bar/config?username=johan") \
      .returns(@ok_stub)
    client.expects(:connection).once.returns(connection_stub)
    File.expects(:exist?).with(@full_real_path).returns(true)
    assert_equal "git-upload-pack '#{@full_real_path}'", client.to_git_shell_argument
  end
end
